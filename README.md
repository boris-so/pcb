Adapter breakouts for 8p8c and 4p4c female sockets to 2.54mm breadboard.

Tools requred:
- #0 - cone cutter 30°x0.1mm, used to engrave paths
- #1 - drill bit d=0.8mm - holes. Pick one of your choise.
- #7 - drill bit d=2.0mm - same for mount holes
- #5 - corn cutter d=1.6mm - outline cutter